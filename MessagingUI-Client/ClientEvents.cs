﻿//-----------------------------------------------------------------------
// <author> 
//     M Aditya 
// </author>
//
// <date> 
//     17-Nov-2018 
// </date>
// 
// <reviewer> 
//     K Durga Prasad Reddy
// </reviewer>
//
// <summary>
//      Some Events present in Client-UI
// <summary>
//
// <copyright file="MessagingUIClient" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
//-----------------------------------------------------------------------

namespace Masti.MessagingUIClient
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using Masti.ImageProcessing;
    using Masti.QualityAssurance;
    using Masti.Messenger;

    /// <summary>
    /// Defines the <see cref="ClientChatScreen" />
    /// </summary>
    public partial class ClientChatScreen : Form
    {
        /// <summary>
        /// Whenever the sendButton is clicked the SendMessage function from the messageHandler is triggered
        /// with appropriate parameters and exeption is shown wherever there is an exception
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void SendButtonClick(object sender, EventArgs e)
        {
            if (!(string.IsNullOrEmpty(messageTestBox.Text) || string.Equals(messageTestBox.Text, "Enter Text Here", StringComparison.Ordinal)))
            {
                try
                {
                    messageHandler.SendMessage(messageTestBox.Text, toIP.ToString(), GetDateTime());
                }
                catch (Exception exe)
                {
                    MastiDiagnostics.LogWarning("No server to send message");
                    MessageBox.Show(exe.Message);
                }
            }
        }

        /// <summary>
        /// The event is triggered whenever the Size of the chatScreen(chatScreenTableLayout) is changed.
        /// The event resizes all the messages inside the chatScreen(chatScreenTableLayout)
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void ChatScreenTableLayoutSizeChanged(object sender, EventArgs e)
        {
            foreach (Control c in chatScreenTableLayout.Controls)
            {
                foreach (Control rtb in c.Controls)
                {
                    if (rtb is RichTextBox)
                    {
                        rtb.Width = 3 * (chatScreenTableLayout.Width) / 5;
                    }
                }
            }
        }

        /// <summary>
        /// The rtb_contentResized
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="ContentsResizedEventArgs"/></param>
        private void RichTextBoxContentResized(object sender, ContentsResizedEventArgs e)
        {
            ((RichTextBox)sender).Height = e.NewRectangle.Height + 5;
        }

        /// <summary>
        /// This event is triggered whenever a new control is added to the chatScreenTableLayout.
        /// This event adjust the scrollbar such that the recetly added control is in focus.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="ControlEventArgs"/></param>
        private void ChatScreenTableLayoutControlAdded(object sender, ControlEventArgs e)
        {
            chatScreenTableLayout.ScrollControlIntoView(e.Control);
        }

        /// <summary>
        /// The Event which triggers whenever th messageTextBox is the active control,
        /// this function empties the text in messageTextBox.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void MessageTestBoxEnter(object sender, EventArgs e)
        {
            ((TextBox)sender).Text = string.Empty;
        }
    }
}

﻿//-----------------------------------------------------------------------
// <author> 
//     Polu Varshith
// </author>
//
// <date> 
//     17-Nov-2018 
// </date>
// 
// <reviewer> 
//     K Durga Prasad Reddy
// </reviewer>
//
// <summary>
//     Handles message sent and receive
// <summary>
//
// <copyright file="MessagingUIClient" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
//-----------------------------------------------------------------------

namespace Masti.MessagingUIClient
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using Masti.ImageProcessing;
    using Masti.QualityAssurance;
    using Masti.Messenger;

    /// <summary>
    /// Defines the <see cref="ClientChatScreen" />
    /// </summary>
    public partial class ClientChatScreen : Form
    {
        /// <summary>
        /// Receiver message handler
        /// </summary>
        /// <param name="message">The message<see cref="string"/></param>
        /// <param name="serverIP">The to IP<see cref="string"/></param>
        /// <param name="clientIP">The from IP<see cref="string"/></param>
        /// <param name="dateTime">The dateTime<see cref="string"/></param>
        public void ReceiverMessageHandler(string message, string serverIP, string clientIP, string dateTime)
        {
            this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), "Server", message, "true");
        }

        /// <summary>
        /// Status handler for sent messages
        /// </summary>
        /// <param name="status">The status<see cref="Messenger.Handler.StatusCode"/></param>
        /// <param name="fromIP">The from IP<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        public void SendingStatusHandlers(Messenger.Handler.StatusCode status, string fromIP, string message)
        {
            // if connection fails
            if (string.Equals(status.ToString(), "ConnectionError", StringComparison.Ordinal))
            {
                MastiDiagnostics.LogError("Server Connection Failed");
                MessageBox.Show("Connection Failed");
            }
            //connection is established
            else if (string.Equals(status.ToString(), "ConnectionEstablished", StringComparison.Ordinal))
            {
                MastiDiagnostics.LogInfo("Connection established with server Successfully");
                this.BeginInvoke(new ChangeConnectButtonDelegate(this.ChangeConnectButton));
            }
            // Message is successfully sent
            else if (string.Equals(status.ToString(), "Success", StringComparison.Ordinal))
            {
                MastiDiagnostics.LogInfo("Message sent successfully");
                this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), "Client", message, "true");
            }
            // Sending Message failed
            else if (string.Equals(status.ToString(), "Failure", StringComparison.Ordinal))
            {
                MastiDiagnostics.LogInfo("Message sent failed");
                this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), "Client", message, "false");
            }
        }
    }
}

﻿//-----------------------------------------------------------------------
// <author> 
//     M Aditya 
// </author>
//
// <date> 
//     17-Nov-2018 
// </date>
// 
// <reviewer> 
//     K Durga Prasad Reddy
// </reviewer>
//
// <summary>
//     Invoke message inside the chat Screen.
// <summary>
//
// <copyright file="MessagingUIClient" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
//-----------------------------------------------------------------------

namespace Masti.MessagingUIClient
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using Masti.ImageProcessing;
    using Masti.Messenger;

    /// <summary>
    /// Defines the <see cref="ClientChatScreen" />
    /// </summary>
    public partial class ClientChatScreen : Form
    {
        /// <summary>
        /// Message is shown in the Client Chat Screen based on the status of the message
        /// </summary>
        /// <param name="IsClientMessage">The IsClientMessage<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        /// <param name="messageSentStatus">The messageSentStatus<see cref="string"/></param>
        public void InvokeMessage(string isClientMessage, string message, string messageSentStatus)
        {
            // if message is empty do not show
            if (!(string.IsNullOrEmpty(message)))
            {
                Panel row = new Panel
                {
                    Dock = DockStyle.Fill
                };
                RichTextBox textMessage = new RichTextBox
                {
                    Size = new Size((3 * chatScreenTableLayout.Width) / 5, 40),
                    Font = new Font("Arial", 12, FontStyle.Regular),
                    ScrollBars = RichTextBoxScrollBars.None,
                    ReadOnly = true,
                    BorderStyle = BorderStyle.FixedSingle
                };

                textMessage.ContentsResized += new ContentsResizedEventHandler(RichTextBoxContentResized);
                Label time = new Label();
                // if message is sent successfully
                if (String.Equals(messageSentStatus, "true", StringComparison.Ordinal))
                {
                    time.Size = new Size(70, 10);
                    time.Text = GetDateTime();
                }
                // if sending message is failed
                else
                {
                    time.Size = new Size(120, 10);
                    time.Text = "Message not delivered";
                }

                time.Font = new Font("Arial", 8, FontStyle.Regular);
                time.TextAlign = ContentAlignment.MiddleCenter;

                // if its Client's message
                if (String.Equals(isClientMessage, "Client", StringComparison.Ordinal))
                {
                    textMessage.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FAFAFA");
                    textMessage.Dock = DockStyle.Right;
                    textMessage.BackColor = System.Drawing.ColorTranslator.FromHtml("#1A237E");
                    textMessage.AppendText(message);
                    time.Dock = DockStyle.Right;
                }
                // if its server's message
                else if (String.Equals(isClientMessage, "Server", StringComparison.Ordinal))
                {
                    textMessage.Dock = DockStyle.Left;
                    textMessage.BackColor = System.Drawing.ColorTranslator.FromHtml("#BDBDBD");
                    textMessage.AppendText(message);
                    time.Dock = DockStyle.Left;
                }

                int index = chatScreenTableLayout.RowCount++;
                this.chatScreenTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, textMessage.Height + 10));
                chatScreenTableLayout.Controls.Add(row, 0, index - 1);
                row.Padding = new Padding(10, 1, 2, 1);
                row.Controls.Add(time);
                row.Controls.Add(textMessage);
                messageTestBox.Text = string.Empty;
            }
        }
    }
}

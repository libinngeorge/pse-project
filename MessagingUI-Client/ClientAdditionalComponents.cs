﻿//-----------------------------------------------------------------------
// <author> 
//     M Aditya 
// </author>
//
// <date> 
//     17-Nov-2018 
// </date>
// 
// <reviewer> 
//     K Durga Prasad Reddy
// </reviewer>
//
// <summary>
//      Additional components required for client-UI
// <summary>
//
// <copyright file="MessagingUIClient" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
//-----------------------------------------------------------------------

namespace Masti.MessagingUIClient
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using System.IO;
    using System.Reflection;
    using Masti.ImageProcessing;
    using Masti.Messenger;

    /// <summary>
    /// Defines the <see cref="ClientChatScreen" />
    /// </summary>
    public partial class ClientChatScreen : Form
    {
        /// <summary>
        /// Loads all the additional components required by the Client-UI
        /// </summary>
        private void GetComponents()
        {
            this.messageTestBox.Enabled = false;
            this.sendButton.Enabled = false;

            this.chatScreenTableLayout.Controls.Clear();
            this.chatScreenTableLayout.RowStyles.Clear();
            this.chatScreenTableLayout.AutoScroll = true;
            this.chatScreenTableLayout.VerticalScroll.Visible = true;
            this.chatScreenTableLayout.VerticalScroll.Maximum = this.chatScreenTableLayout.Height;
            this.chatScreenTableLayout.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);
            this.chatScreenTableLayout.BackColor = System.Drawing.ColorTranslator.FromHtml("#FBE9E7");

            this.splitContainer1.Panel1.BackColor = System.Drawing.ColorTranslator.FromHtml("#607D8B");
            Assembly myAssembly = Assembly.GetExecutingAssembly();
            Stream myStream = myAssembly.GetManifestResourceStream("Masti.MessagingUIClient.Images.Send.png");
            this.sendButton.Image = Image.FromStream(myStream);
            this.splitContainer1.FixedPanel = FixedPanel.Panel1;
            // name of Software
            Label name = new Label
            {
                Text = "MASTI",
                BackColor = Color.DarkSlateGray,
                ForeColor = Color.Transparent,
                Font = new Font("Arial", 35, FontStyle.Regular),
                TextAlign = ContentAlignment.MiddleCenter,
                Dock = DockStyle.Fill,
                Visible = true
            };
            this.logoPanel.Controls.Add(name);

            Panel navigation = new Panel
            {
                Dock = DockStyle.Left,
                Width = 23,
                Visible = true,
            };
            navigation.SizeChanged += new EventHandler(NavigationPanelSizeChanged);
            this.splitContainer1.Panel2.Controls.Add(navigation);
            myStream = myAssembly.GetManifestResourceStream("Masti.MessagingUIClient.Images.minimizeConnectPanel.png");

            PictureBox navigationImage = new PictureBox
            {
                Location = new Point(0, (2 * splitContainer1.Panel2.Height) / 5),
                Image = Image.FromStream(myStream)
            };
            navigationImage.Click += new EventHandler(NavigationImageClick);
            navigation.Controls.Add(navigationImage);
        }
    }
}

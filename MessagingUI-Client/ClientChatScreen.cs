﻿//-----------------------------------------------------------------------
// <author> 
//     M Aditya 
// </author>
//
// <date> 
//     17-Nov-2018 
// </date>
// 
// <reviewer> 
//     K Durga Prasad Reddy
// </reviewer>
// 
// <summary>
//     Delegates and class members.
// <summary>
//
// <copyright file="MessagingUIClient" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
//-----------------------------------------------------------------------

namespace Masti.MessagingUIClient
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Masti.ImageProcessing;
    using Masti.Messenger;

    /// <summary>
    /// Defines the <see cref="ClientChatScreen" />
    /// </summary>
    public partial class ClientChatScreen : Form
    {
        /// <summary>
        /// Defines the Messaging module object which is implementing the interface.
        /// </summary>
        private IUXMessage messageHandler = null;

        /// <summary>
        /// Defines the image Handler.
        /// </summary>
        private ImageClient imageHandler = null;

        /// <summary>
        /// The toIP defines the IP-address of the server machine.
        /// </summary>
        private IPAddress toIP = null;

        /// <summary>
        /// The toPort defines the server port, initialized to zero.
        /// </summary>
        private int toPort = 0;

        /// <summary>
        /// Defines the uname which refers to the username/client Name.
        /// </summary>
        private string uname = "";

        /// <summary>
        /// connectPanelVisible indicates whether the connectPanel is visible or not
        /// </summary>
        internal bool connectPanelVisible = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientChatScreen"/> class.
        /// </summary>
        public ClientChatScreen()
        {
            InitializeComponent();
            GetComponents();
        }

        /// <summary>
        /// The Delegate for InvokeMessage function 
        /// </summary>
        /// <param name="isClientMessage">checks if Client Message or Server Message<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        /// <param name="messageSentSender">Status of the message("Success" or "Failure")<see cref="string"/></param>
        internal delegate void InvokeMessageDelegate(string isClientMessage, string message, string messageSentStatus);

        /// <summary>
        /// The Delegate for changeConnectButton function
        /// </summary>
        internal delegate void ChangeConnectButtonDelegate();       
    }
}

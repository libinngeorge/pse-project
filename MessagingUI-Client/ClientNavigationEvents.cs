﻿//-----------------------------------------------------------------------
// <author> 
//     M Aditya 
// </author>
//
// <date> 
//     17-Nov-2018 
// </date>
// 
// <reviewer> 
//     K Durga Prasad Reddy
// </reviewer>
//
// <summary>
//      All controls required to dock connectPanel
// <summary>
//
// <copyright file="MessagingUIClient" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
//-----------------------------------------------------------------------

namespace Masti.MessagingUIClient
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Reflection;
    using System.Windows.Forms;
    using Masti.ImageProcessing;
    using Masti.Messenger;

    /// <summary>
    /// Defines the <see cref="ClientChatScreen" />
    /// </summary>
    public partial class ClientChatScreen : Form
    {
        /// <summary>
        /// The Method to change the image displpayed on the navigationImage whenever it is clicked
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void NavigationImageClick(object sender, EventArgs e)
        {
            Assembly myAssembly = Assembly.GetExecutingAssembly();
            PictureBox pictureBox = sender as PictureBox;
            if (connectPanelVisible)
            {
                Stream myStream = myAssembly.GetManifestResourceStream("Masti.MessagingUIClient.Images.MaximizeConnectPanel.png");
                pictureBox.Image = Image.FromStream(myStream);
                //connectPanel is collapsed
                this.splitContainer1.Panel1Collapsed = true;
                connectPanelVisible = false;
            }
            else
            {
                Stream myStream = myAssembly.GetManifestResourceStream("Masti.MessagingUIClient.Images.MinimizeConnectPanel.png");
                pictureBox.Image = Image.FromStream(myStream);
                //connectPanel is restored
                this.splitContainer1.Panel1Collapsed = false;
                connectPanelVisible = true;
            }
        }

        /// <summary>
        /// To change the position/location of the navigationImage whenever the 
        /// screen size is adjusted or resized.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void NavigationPanelSizeChanged(object sender, EventArgs e)
        {
            foreach (Control pb in ((Panel)sender).Controls)
            {
                if (pb is PictureBox)
                {
                    pb.Location = new Point(0, 2 * (splitContainer1.Panel2.Height) / 5);
                }
            }
        }
    }
}

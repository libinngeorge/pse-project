﻿//-----------------------------------------------------------------------
// <author> 
//     M Aditya and Polu Varshith
// </author>
//
// <date> 
//     17-Nov-2018 
// </date>
// 
// <reviewer> 
//     K Durga Prasad Reddy
// </reviewer>
//
// <summary>
//      Connecting the client with the server
// <summary>
//
// <copyright file="MessagingUIClient" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
//-----------------------------------------------------------------------

namespace Masti.MessagingUIClient
{
    using System;
    using System.Drawing;
    using System.Net;
    using System.Windows.Forms;
    using Masti.ImageProcessing;
    using Masti.QualityAssurance;
    using Masti.Messenger;

    /// <summary>
    /// Defines the <see cref="ClientChatScreen" />
    /// </summary>
    public partial class ClientChatScreen : Form
    {
        /// <summary>
        /// When connect button is clicked validation is done and if any error/exception is 
        /// triggered it is shown appropriately If no error/exception connection is established
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void ConnectButtonClick(object sender, EventArgs e)
        {
            //check if required fields are filled
            if (String.IsNullOrEmpty(clientNameTextBox.Text) || String.IsNullOrEmpty(serverIPTextBox.Text) || String.IsNullOrEmpty(serverPortTextBox.Text))
            {
                this.errorLabel.Text = "All fields are compulsory";
                this.errorLabel.Font = new Font("Arial", 10, FontStyle.Regular);
                return;
            }
            else
            {
                if (string.Equals(connectButton.Text, "Connect", StringComparison.Ordinal))
                {
                    this.errorLabel.Text = String.Empty;
                    // checks if server IP is valid
                    try
                    {
                        toIP = IPAddress.Parse(serverIPTextBox.Text);
                    }
                    catch (Exception)
                    {
                        MastiDiagnostics.LogError("Invalid Server IP");
                        errorLabel.Text = "Invalid Server IP/Port";
                        return;
                    }
                    // checks if server port is valid 
                    try
                    {
                        toPort = int.Parse(serverPortTextBox.Text, null);
                    }
                    catch (Exception)
                    {
                        MastiDiagnostics.LogError("Invalid Server Port Number");
                        this.errorLabel.Text = "Invalid Server IP/Port";
                        return;
                    }
                    uname = clientNameTextBox.Text;
                    messageHandler = new Messager(toIP.ToString(), toPort, uname);
                    imageHandler = FactoryImageClient.CreateImageClient(toIP.ToString(), toPort);
                    messageHandler.SubscribeToDataReceiver(ReceiverMessageHandler);
                    messageHandler.SubscribeToStatusReceiver(SendingStatusHandlers);

                    // establish the connection with the server
                    try
                    {
                        messageHandler.Connectify(uname, toIP.ToString());
                    }
                    catch (Exception exe)
                    {
                        MessageBox.Show(exe.Message);
                    }
                    this.clientNameTextBox.ReadOnly = true;
                    this.serverIPTextBox.ReadOnly = true;
                    this.serverPortTextBox.ReadOnly = true;
                    this.messageTestBox.Enabled = true;
                    this.sendButton.Enabled = true;
                }
            }
        }
    }
}

﻿//-----------------------------------------------------------------------
// <author> 
//     Prabal Vashisht
// </author>
//
// <date> 
//     25/10/2018
// </date>
// 
// <reviewer> 
//     Prabal Vashisht 
// </reviewer>
// 
// <copyright file="Persistence.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This module is used to save, retrieve and delete messages from the database.
// </summary>
//-----------------------------------------------------------------------
namespace Masti.Persistence
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Masti.QualityAssurance;
    using MongoDB.Driver;
    
    /// <summary>
    /// Persistence class is used to save, retrieve and delete messages from the database.
    /// </summary>
    public class Persistence : IPersistence
    {
        /// <summary>
        /// Variable stores the address of the database server.
        /// </summary>
        private const string ConnectionString = "mongodb://localhost";

        /// <summary>
        /// Variable stores the name of the database in MongoDB.
        /// </summary>
        private const string DatabaseName = "Persistence";

        /// <summary>
        /// Variable stores the name of the collection in the database.
        /// </summary>
        private const string CollectionName = "Messages";

        /// <summary>
        /// Variable is used to form a connection to the database.
        /// </summary>
        private MongoClient client;

        /// <summary>
        /// Saves the message, tagged with an appropriate session ID, in the database.
        /// </summary>
        /// <param name="message">Message to be stored</param>
        /// <returns>Returns success (true) or failure (false)</returns>
        public bool SaveSession(string message)
        {
            QualityAssurance.MastiDiagnostics.LogInfo("Saving session...");
            this.client = new MongoClient(ConnectionString);
            try
            {
                IMongoDatabase database = this.client.GetDatabase(DatabaseName);
                IMongoCollection<Document> collection = database.GetCollection<Document>(CollectionName);

                // Invariant is maintained that one more than the number of documents in database is the current session ID.
                long numDocuments = collection.Find(x => true).ToList().Count();

                collection.InsertOne(new Document { SessionId = (int)(numDocuments + 1), Message = message });
            }
            catch (MongoException ex)
            {
                QualityAssurance.MastiDiagnostics.LogError(ex.Message);
                return false; 
            }
            catch (NullReferenceException ex)
            {
                QualityAssurance.MastiDiagnostics.LogError(ex.Message);
                return false;
            }

            QualityAssurance.MastiDiagnostics.LogInfo("Session saved successfully!");
            return true;
        }

        /// <summary>
        /// Returns a list (from beginning till the end) of messages from the database.
        /// </summary>
        /// <param name="startSessionId">Beginning session ID</param>
        /// <param name="endSessionId">Ending session ID</param>
        /// <returns>List of messages</returns>
        public Collection<string> RetrieveSession(int startSessionId, int endSessionId)
        {
            QualityAssurance.MastiDiagnostics.LogInfo("Retrieving messages...");
            if (startSessionId > endSessionId)
            {
                QualityAssurance.MastiDiagnostics.LogWarning("Start session ID is greater than the end session ID!");
                return null;
            }

            if (startSessionId <= 0 || endSessionId <= 0)
            {
                QualityAssurance.MastiDiagnostics.LogWarning("Start session ID or end session ID is negative!");
                return null;
            }
            
            this.client = new MongoClient(ConnectionString);
            Collection<string> returnMessages = new Collection<string>();
            try
            {
                IMongoDatabase database = this.client.GetDatabase(DatabaseName);
                IMongoCollection<Document> collection = database.GetCollection<Document>(CollectionName);

                // Invariant is maintained that one more than the number of documents in database is the current session ID.
                long numDocuments = collection.Find(x => true).ToList().Count();

                for (int i = startSessionId; i <= endSessionId && i <= numDocuments; i++)
                {
                    var result = collection.Find(x => x.SessionId == i).ToList()[0];
                    returnMessages.Add(result.Message);
                }
            }
            catch (MongoException ex)
            {
                QualityAssurance.MastiDiagnostics.LogError(ex.Message);
                return null;
            }
            catch (NullReferenceException ex)
            {
                QualityAssurance.MastiDiagnostics.LogError(ex.Message);
                return null;
            }

            return returnMessages;
        }

        /// <summary>
        /// Delete messages (from the beginning till the end) from the database.
        /// </summary>
        /// <param name="startSessionId">Beginning session ID</param>
        /// <param name="endSessionId">Ending session ID</param>
        /// <returns>Number of sessions deleted</returns>
        public int DeleteSession(int startSessionId, int endSessionId)
        {
            if (startSessionId > endSessionId)
            {
                QualityAssurance.MastiDiagnostics.LogWarning("Start session ID is greater than the end session ID!");
                return -1;
            }

            if (startSessionId <= 0 || endSessionId <= 0)
            {
                QualityAssurance.MastiDiagnostics.LogWarning("Start session ID or end session ID is negative!");
                return -1;
            }

            int numDocumentsDeleted = 0;
            try
            {
                this.client = new MongoClient(ConnectionString);
                IMongoDatabase database = this.client.GetDatabase(DatabaseName);
                IMongoCollection<Document> collection = database.GetCollection<Document>(CollectionName);

                // Invariant is maintained that one more than the number of documents in database is the current session ID.
                long numDocuments = collection.Find(x => true).ToList().Count();

                for (int i = startSessionId; i <= endSessionId && i <= numDocuments; i++)
                {
                    DeleteResult result = collection.DeleteOne(x => x.SessionId == i);
                    bool deleted = result.IsAcknowledged;

                    if (deleted == true)
                    {
                        numDocumentsDeleted += 1;
                    }
                }

                if (endSessionId < numDocuments)
                {
                    // Rename the session IDs
                    for (int i = endSessionId + 1; i <= numDocuments; i++)
                    {
                        var filter = Builders<Document>.Filter.Eq("SessionId", i);
                        var update = Builders<Document>.Update.Set("SessionId", startSessionId - 1 + (i - endSessionId));
                        collection.UpdateOne(filter, update);
                    }
                }
            }
            catch (MongoException ex)
            {
                QualityAssurance.MastiDiagnostics.LogError(ex.Message);
                return -1;
            }
            catch (NullReferenceException ex)
            {
                QualityAssurance.MastiDiagnostics.LogError(ex.Message);
                return -1;
            }

            return numDocumentsDeleted;
        }

        /// <summary>
        /// Returns the current session ID.
        /// </summary>
        /// <returns>Current session ID</returns>
        public int GetCurrentSessionId()
        {
            try
            {
                this.client = new MongoClient(ConnectionString);
                IMongoDatabase database = this.client.GetDatabase(DatabaseName);
                IMongoCollection<Document> collection = database.GetCollection<Document>(CollectionName);

                // Invariant is maintained that one more than the number of documents in database is the current session ID.
                long numDocuments = collection.Find(x => true).ToList().Count();
                return (int)numDocuments + 1;
            }
            catch (MongoException ex)
            {
                QualityAssurance.MastiDiagnostics.LogError(ex.Message);
                return 0;
            }
            catch (NullReferenceException ex)
            {
                QualityAssurance.MastiDiagnostics.LogError(ex.Message);
                return 0;
            }
            catch(Exception ex)
            {
                QualityAssurance.MastiDiagnostics.LogError(ex.Message);
                Console.WriteLine(ex.Message);
                return 0;
            }
        }
    }
}

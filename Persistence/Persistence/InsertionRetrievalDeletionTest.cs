﻿//-----------------------------------------------------------------------
// <author> 
//     Prabal Vashisht (prabalv04@gmail.com)
// </author>
//
// <date> 
//     12/11/2018
// </date>
// 
// <reviewer>
//      Prabal Vashisht (prabalv04@gmail.com)
// </reviewer>
// 
// <copyright file="InsertionRetrievalDeletionTest.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      Test for the Test Harness. 
// </summary>
//-----------------------------------------------------------------------

namespace Masti.Persistence
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Masti.QualityAssurance;

    /// <summary>
    /// Define a Test to check if 
    /// class functions properly.
    /// </summary>
    public class InsertionRetrievalDeletionTest : ITest
    {
        /// <summary>
        /// The logger is used for Logging functionality within the test.
        /// Helps in debugging of tests.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="InsertionRetrievalDeletionTest" /> class.
        /// </summary>
        /// <param name="logger">Assigns the Logger to be used by the Test</param>
        public InsertionRetrievalDeletionTest(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Describe the function test defined by the module developer.
        /// </summary>
        /// <returns>Returns whether the status is successful or not.</returns>
        public bool Run()
        {
            Persistence persistenceTest = new Persistence();

            // Insertion check (Three strings will be inserted inside the database).
            this.logger.LogInfo("Insertion started !");
            bool inserted1 = persistenceTest.SaveSession("1");
            bool inserted2 = persistenceTest.SaveSession("2");
            bool inserted3 = persistenceTest.SaveSession("3");

            if (inserted1 && inserted2 && inserted3)
            {
                this.logger.LogSuccess("Insertion succeeded !");
            }
            else
            {
                this.logger.LogError("Insertion failed !");
                return false;
            }

            // Retrieval check
            this.logger.LogInfo("Retrieval started !");

            // greaterThanCheck checks if the starting session ID is less than the ending session ID or not.
            bool greaterThanCheck;

            // posStartCheck checks if the starting session ID is greater than 0 or not.
            bool posStartCheck;

            // posEndCheck checks if the ending session ID is greater than 0 or not.
            bool posEndCheck;

            // retrieveCheck checks the primary functionality of the retrieve function.
            bool retrieveCheck;
            Collection<string> retListString = persistenceTest.RetrieveSession(3, 2);

            if (retListString == null)
            {
                greaterThanCheck = true;
            }
            else
            {
                greaterThanCheck = false;
            }

            retListString = persistenceTest.RetrieveSession(0, 5);

            if (retListString == null)
            {
                posStartCheck = true;
            }
            else
            {
                posStartCheck = false;
            }

            retListString = persistenceTest.RetrieveSession(5, 0);

            if (retListString == null)
            {
                posEndCheck = true;
            }
            else
            {
                posEndCheck = false;
            }

            int currSessionID = persistenceTest.GetCurrentSessionId();
            retListString = persistenceTest.RetrieveSession(currSessionID - 3, currSessionID - 2);

            if (retListString[0] == "1" && retListString[1] == "2")
            {
                retrieveCheck = true;
            }
            else
            {
                retrieveCheck = false;
            }

            if (greaterThanCheck && posStartCheck && posEndCheck && retrieveCheck)
            {
                this.logger.LogSuccess("Retrieval succeeded !");
            }
            else
            {
                this.logger.LogError("Retrieval failed !");
                return false;
            }

            // Deletion check (Deletes "1" and "2" stored previously)
            this.logger.LogInfo("Deletion started!");
        
            // deleteCheck checks the primary functionality of the delete function i.e. to delete and fix the session IDs accordingly.
            bool deleteCheck;
            int numDeleted = persistenceTest.DeleteSession(3, 2);

            if (numDeleted == -1)
            {
                greaterThanCheck = true;
            }
            else
            {
                greaterThanCheck = false;
            }

            numDeleted = persistenceTest.DeleteSession(0, 5);

            if (numDeleted == -1)
            {
                posStartCheck = true;
            }
            else
            {
                posStartCheck = false;
            }

            numDeleted = persistenceTest.DeleteSession(5, 0);

            if (numDeleted == -1)
            {
                posEndCheck = true;
            }
            else
            {
                posEndCheck = false;
            }

            numDeleted = persistenceTest.DeleteSession(currSessionID - 3, currSessionID - 2);
            retListString = persistenceTest.RetrieveSession(currSessionID - 3, currSessionID - 3);

            if (retListString[0] == "3" && numDeleted == 2)
            {
                deleteCheck = true;
            }
            else
            {
                deleteCheck = false;
            }

            if (greaterThanCheck && posStartCheck && posEndCheck && deleteCheck)
            {
                this.logger.LogSuccess("Deletion succeeded!");
                this.logger.LogSuccess("Test cases passed");
                return true;
            }
            else
            {
                this.logger.LogError("Deletion failed !");
                return false;
            }
        }
    }
}

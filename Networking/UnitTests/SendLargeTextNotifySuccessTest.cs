﻿// -----------------------------------------------------------------------
// <author> 
//      Jude K Anil
// </author>
//
// <date> 
//      03-11-2018 
// </date>
// 
// <reviewer>
//      Libin N George
//      Parth Patel
//      Rohith Reddy
// </reviewer>
//
// <copyright file="SendLargeTextNotifySuccessTest.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary> 
//      This file is a part of Networking Module Unit Testing.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// Test sends messages with increasing length of text.
    /// The test fails if there is a mismatch between the send
    /// and received messages or if there is a timeout exception.
    /// Both Professor side and student side communicator.
    /// </summary>
    public class SendLargeTextNotifySuccessTest : ITest
    {
        /// <summary>
        /// Event signaled when ProfReceivalHandler is called.
        /// The Run function waits for this event after the student communicator calls Send function.
        /// </summary>
        private static ManualResetEvent profEvent = new ManualResetEvent(false);

        /// <summary>
        /// Event signaled when studentReceivalHandler is called.
        /// The Run function waits for this event after the professor communicator calls Send function.
        /// </summary>
        private static ManualResetEvent studentEvent = new ManualResetEvent(false);

        /// <summary>
        /// Event signaled when text received does not match the text sent.
        /// The Run function throws an exception if triggered.
        /// </summary>
        private static ManualResetEvent textDoesNotMatchEvent = new ManualResetEvent(false);

        /// <summary>
        /// Loopback address as Professor's IP address.
        /// </summary>
        private IPAddress profIP = IPAddress.Parse("169.254.140.227");

        /// <summary>
        /// Stores the result of the test. True for Success, false otherwise.
        /// </summary>
        private bool result = true;

        /// <summary>
        /// Stores the duration for which the thread should wait before triggering timeout.
        /// </summary>
        private TimeSpan interval = new TimeSpan(hours: 0, minutes: 0, seconds: 2);

        /// <summary>
        /// Message text to be sent.
        /// </summary>
        private StringBuilder text = new StringBuilder("a");

        /// <summary>
        /// Stores the schema object.
        /// </summary>
        private ISchema schema = new MessageSchema();

        /// <summary>
        /// Stores the logger object.
        /// </summary>
        private ILogger logger;

        /// <summary>
        /// Maximum number of attempts at sending a message.
        /// </summary>
        private int maxRetries = 5;

        /// <summary>
        /// Maximum time awaited for acknowledgement receival.
        /// </summary>
        private int waitTimeForAcknowledge = 5;

        /// <summary>
        /// Stores the professor's port.
        /// </summary>
        private int profPort = 1252;

        /// <summary>
        /// Stores the size by which the text length is increased.
        /// </summary>
        private int padLength = 102400;

        /// <summary>
        /// Initializes a new instance of the <see cref="SendLargeTextNotifySuccessTest" /> class
        /// </summary>
        /// <param name="logger">Stores the logger object given.</param>
        public SendLargeTextNotifySuccessTest(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Executes the test and is part of ITest interface. It is called by the test harness.
        /// </summary>
        /// <returns>True if test is success, false otherwise.</returns>
        public bool Run()
        {
            string log;

            using (Communication profCommunicator = new Communication(
                                                      port: this.profPort,
                                                      maxRetries: this.maxRetries,
                                                      waitTimeForAcknowledge: this.waitTimeForAcknowledge))
            {
                using (Communication studentCommunicator = new Communication(
                                                         ipAddress: IPAddress.Parse(profCommunicator.LocalIP),
                                                         port: this.profPort,
                                                         maxRetries: this.maxRetries,
                                                         waitTimeForAcknowledge: this.waitTimeForAcknowledge))
                {
                    try
                    {
                        this.profIP = IPAddress.Parse(profCommunicator.LocalIP);
                        studentCommunicator.SubscribeForDataStatus(type: DataType.Message, statusHandler: this.StudentStatusHandler);
                        this.logger.LogInfo(string.Format(
                                                          provider: CultureInfo.CurrentCulture,
                                                          format: "student communicator subscribed for data status."));

                        studentCommunicator.SubscribeForDataReceival(DataType.Message, this.StudentReceivalHandler);
                        this.logger.LogInfo(string.Format(
                                                          provider: CultureInfo.CurrentCulture,
                                                          format: "student communicator subscribed for data Receival."));

                        profCommunicator.SubscribeForDataStatus(DataType.Message, this.ProfStatusHandler);
                        this.logger.LogInfo(string.Format(
                                                          provider: CultureInfo.CurrentCulture,
                                                          format: "Professor communicator subscribed for data status."));

                        profCommunicator.SubscribeForDataReceival(type: DataType.Message, receivalHandler: this.ProfReceivalHandler);
                        this.logger.LogInfo(string.Format(
                                                          provider: CultureInfo.CurrentCulture,
                                                          format: "Professor communicator subscribed for data Receival."));

                        string padString = new string('a', this.padLength);

                        for (int i = 0; i < 40; i += 1)
                        {
                            // Encoding the text as per schema.
                            Dictionary<string, string> dict = new Dictionary<string, string>();
                            dict.Add("Messaging", this.text.ToString());
                            string message = this.schema.Encode(dict);
                            this.logger.LogInfo(string.Format(
                                                          provider: CultureInfo.CurrentCulture,
                                                          format: "Text encoded by the Schema object."));

                            // Reset the event to wait for receival of the sent message by the profReceivalHandler.
                            profEvent.Reset();

                            // Reset the event to wait for receival of the sent message by the studentReceivalHandler.
                            studentEvent.Reset();

                            studentCommunicator.Send(msg: message, targetIP: this.profIP, type: DataType.Message);
                            log = $"Student communicator called send function " +
                                $"with message of length: {message.Length.ToString(CultureInfo.CurrentCulture)}";
                            this.logger.LogInfo(string.Format(provider: CultureInfo.CurrentCulture, format: log));

                            if (!profEvent.WaitOne(this.interval))
                            {
                                this.logger.LogWarning(string.Format(CultureInfo.CurrentCulture, "ProfEvent Not Triggered."));
                                throw new TimeoutException();
                            }
                            
                            if (textDoesNotMatchEvent.WaitOne(0))
                            {
                                this.logger.LogError(string.Format(
                                    CultureInfo.CurrentCulture,
                                    "Text received by ProfReceivalHandler is not same as the text sent."));
                                throw new TextDoesNotMatchException();
                            }

                            profCommunicator.Send(msg: message, targetIP: this.profIP, type: DataType.Message);
                            log = $"prof communicator called send function" +
                                $" with message of length: {message.Length.ToString(CultureInfo.CurrentCulture)}";
                            this.logger.LogInfo(string.Format(
                                                          provider: CultureInfo.CurrentCulture,
                                                          format: log));

                            if (!studentEvent.WaitOne(this.interval))
                            {
                                this.logger.LogWarning(string.Format(
                                    CultureInfo.CurrentCulture, "studentEvent Not Triggered."));
                                throw new TimeoutException();
                            }

                            if (textDoesNotMatchEvent.WaitOne(0))
                            {
                                this.logger.LogError(string.Format(
                                    CultureInfo.CurrentCulture,
                                    "Text received by StudentReceivalHandler is not same as the text sent."));
                                throw new TextDoesNotMatchException();
                            }

                            this.text.Append(padString);
                        }
                    }
                    catch (TimeoutException t)
                    {
                        log = $"Time out Exception caught. Exception raised is: {t.ToString()}";
                        this.logger.LogError(string.Format(provider: CultureInfo.CurrentCulture, format: log));

                        this.result = false;
                    }
                    catch (TextDoesNotMatchException r)
                    {
                        log = $"Text mismatch Exception causght. Exception raised is: {r.ToString()}";
                        this.logger.LogError(string.Format(provider: CultureInfo.CurrentCulture, format: log));

                        this.result = false;
                    }
                }
            }
            
            return this.result;
        }

        /// <summary>
        /// Delegate function to be called by the student communication module.
        /// It is called to notify status of message waiting to be sent by the communicator.
        /// </summary>
        /// <param name="data">Stores the data of the message.</param>
        /// <param name="statusCode">Stores the status of the message to be sent.</param>
        private void StudentStatusHandler(string data, StatusCode statusCode)
        {
            string log = $"StudentStatusHandler called with status: {statusCode.ToString()}";
            this.logger.LogInfo(string.Format(provider: CultureInfo.CurrentCulture, format: log));

            if (statusCode.Equals(StatusCode.Failure))
            {
                this.result = false;
            }

            return;
        }

        /// <summary>
        /// Delegate function to be called by the professor communication module.
        /// It is called to notify status of message waiting to be sent by the communicator.
        /// </summary>
        /// <param name="data">Stores the data of the message.</param>
        /// <param name="statusCode">Stores the status of the message to be sent.</param>
        private void ProfStatusHandler(string data, StatusCode statusCode)
        {
            string log = $"ProfStatusHandler called with status: {statusCode.ToString()}";
            this.logger.LogInfo(string.Format(provider: CultureInfo.CurrentCulture, format: log));

            if (statusCode.Equals(StatusCode.Failure))
            {
                this.result = false;
            }

            return;
        }

        /// <summary>
        /// Delegate function to be called by the student communication module.
        /// It is called when the communicator receives a new message.
        /// </summary>
        /// <param name="data">Stores the data of the message received.</param>
        /// <param name="fromIP">Stores the IP address from which the message was received.</param>
        private void StudentReceivalHandler(string data, IPAddress fromIP)
        {
            string log = $"StudentReceivalHandler called with data from IPAddress: {fromIP.ToString()}";
            this.logger.LogInfo(string.Format(provider: CultureInfo.CurrentCulture, format: log));

            IDictionary<string, string> dict = this.schema.Decode(data, false);
            string receivedText = dict["Messaging"];

            if (!receivedText.Equals(this.text.ToString(), StringComparison.Ordinal))
            {
                this.logger.LogError(string.Format(
                                                  provider: CultureInfo.CurrentCulture,
                                                  format: $"Sent text: {this.text}; Received text: {dict["Messaging"]}"));
                textDoesNotMatchEvent.Set();
            }

            studentEvent.Set();
            return;
        }

        /// <summary>
        /// Delegate function to be called by the professore communication module.
        /// It is called when the communicator receives a new message.
        /// </summary>
        /// <param name="data">Stores the data of the message received.</param>
        /// <param name="fromIP">Stores the IP address from which the message was received.</param>
        private void ProfReceivalHandler(string data, IPAddress fromIP)
        {
            string log = $"ProfReceivalHandler called with data from IPAddress: {fromIP.ToString()}";
            this.logger.LogInfo(string.Format(provider: CultureInfo.CurrentCulture, format: log));

            IDictionary<string, string> dict = this.schema.Decode(data, false);
            string receivedText = dict["Messaging"];

            if (!receivedText.Equals(this.text.ToString(), StringComparison.Ordinal))
            {
                this.logger.LogError(string.Format(
                                                  provider: CultureInfo.CurrentCulture,
                                                  format: $"Sent text: {this.text}; Received text: {dict["Messaging"]}"));

                textDoesNotMatchEvent.Set();
            }

            profEvent.Set();
            return;
        }
    }
}

﻿// -----------------------------------------------------------------------
// <author> 
//      Jude K Anil
// </author>
//
// <date> 
//      03-11-2018 
// </date>
// 
// <reviewer>
//      Libin N George
//      Parth Patel
//      Ayush Mittal
// </reviewer>
//
// <copyright file="SendInvalidIPNotifyFailure1Test.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary> 
//      This file is a part of Networking Module Unit Testing.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// Test sends message with text but with invalid IP.
    /// The test fails if Failure status is not assigned as
    /// status in the status handler or if timeout exception
    /// is triggered. The sending communicator module used
    /// is the student side communicator.
    /// </summary>
    public class SendInvalidIPNotifyFailure1Test : ITest
    {
        /// <summary>
        /// Event is signaled when statuslHandler is called.
        /// The Run function waits on this event.
        /// </summary>
        private static ManualResetEvent statusEvent = new ManualResetEvent(false);

        /// <summary>
        /// Invalid IP as target IP address.
        /// </summary>
        private IPAddress targetIP = IPAddress.Parse("111.111.0.1");

        /// <summary>
        /// The port at which the communicator listens.
        /// </summary>
        private int listeningPort = 1250;

        /// <summary>
        /// Message text to be sent.
        /// </summary>
        private string text = "Hi";

        /// <summary>
        /// Stores the result of the test. True for Success, false otherwise.
        /// </summary>
        private bool result = false;

        /// <summary>
        /// Stores the duration for which the thread should sleep after calling send function of communicator class.
        /// </summary>
        private TimeSpan interval = new TimeSpan(hours: 0, minutes: 0, seconds: 10);

        /// <summary>
        /// Stores the schema object.
        /// </summary>
        private ISchema schema = new MessageSchema();

        /// <summary>
        /// Stores the logger object.
        /// </summary>
        private ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="SendInvalidIPNotifyFailure1Test" /> class
        /// </summary>
        /// <param name="logger">Stores the logger object given.</param>
        public SendInvalidIPNotifyFailure1Test(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Executes the test and is part of ITest interface. It is called by the test harness.
        /// </summary>
        /// <returns>True if test is success, false otherwise.</returns>
        public bool Run()
        {
            IPAddress serverIP;
            using (Communication serverCommunicator = new Communication())
            {
                serverIP = IPAddress.Parse(serverCommunicator.LocalIP);
            }

            using (Communication communicator = new Communication(serverIP, this.listeningPort, 2, 1))
            {
                try
                {
                    communicator.SubscribeForDataStatus(type: DataType.Message, statusHandler: this.StatusHandler);
                    this.logger.LogInfo(string.Format(CultureInfo.CurrentCulture, "Subscribed for data status."));

                    communicator.SubscribeForDataReceival(type: DataType.Message, receivalHandler: this.ReceivalHandler);
                    this.logger.LogInfo(string.Format(CultureInfo.CurrentCulture, "Subscribed for data Receival."));

                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add("Messaging", this.text);

                    string message = this.schema.Encode(dict);
                    this.logger.LogInfo(string.Format(CultureInfo.CurrentCulture, "Text encoded by the Schema object."));

                    // Reset the event to wait for receival of the send message.
                    statusEvent.Reset();

                    communicator.Send(msg: message, targetIP: this.targetIP, type: DataType.Message);
                    this.logger.LogInfo(string.Format(CultureInfo.CurrentCulture, "Send function called."));

                    if (!statusEvent.WaitOne(this.interval))
                    {
                        this.logger.LogWarning(string.Format(CultureInfo.CurrentCulture, "StatusEvent not triggered."));
                        throw new TimeoutException();
                    }
                }
                catch (TimeoutException t)
                {
                    this.logger.LogInfo(string.Format(
                                                      provider: CultureInfo.CurrentCulture,
                                                      format: $"Time out Exception caught. Exception raised is: {t.ToString()}"));
                    this.result = false;
                }
            }

            return this.result;
        }

        /// <summary>
        /// Delegate function to be called by the communication module. It is
        /// called to notify status of message waiting to be sent by the communicator.
        /// </summary>
        /// <param name="data">Stores the data of the message.</param>
        /// <param name="statusCode">Stores the status of the message to be sent.</param>
        private void StatusHandler(string data, StatusCode statusCode)
        {
            string log = $"StatusHandler called with data: {data} and status: {statusCode.ToString()}";
            this.logger.LogInfo(string.Format(provider: CultureInfo.CurrentCulture, format: log));

            if (statusCode.Equals(StatusCode.Failure))
            {
                this.result = true;
            }

            statusEvent.Set();

            return;
        }

        /// <summary>
        /// Delegate function to be called by the communication module. It is
        /// called when the communicator receives a new message.
        /// </summary>
        /// <param name="data">Stores the data of the message received.</param>
        /// <param name="fromIP">Stores the IP address from which the message was received.</param>
        private void ReceivalHandler(string data, IPAddress fromIP)
        {
            string log = $"ReceivalHandler called with data: {data} and IPAddress: {fromIP.ToString()}";
            this.logger.LogWarning(string.Format(provider: CultureInfo.CurrentCulture, format: log));

            return;
        }
    }
}

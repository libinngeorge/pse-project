﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A, A Vinay Krishna and Polu Varshith
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Delegates and class members.
// </summary>
// <copyright file="ServerChatScreen.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Resources;
    using System.Windows.Forms;
    using Masti.ImageProcessing;
    using Masti.QualityAssurance;
    using Masti.Messenger;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {

        /// <summary>
        /// Messaging module object which is implementing the interface.
        /// </summary>
        private IUXMessage messageHandler = null;

        /// <summary>
        /// Defines the image handler.
        /// </summary>
        private IImageProcessing imageHandler = null;

        /// <summary>
        /// Defines the server port.
        /// </summary>
        private int serverPort = 0;

        /// <summary>
        /// Dictionary which contains client usernames and their IP's.
        /// </summary>
        private Dictionary<string, string> userNameIPMap = new Dictionary<string, string>();

        /// <summary>
        /// Defines the previous client who was sharing the screen.
        /// </summary>
        private TabPage previousScreenSharingClient = null;

        /// <summary>
        /// Check if the message is present in reply box.
        /// </summary>
        private bool messageInFocus = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerChatScreen"/> class.
        /// </summary>
        public ServerChatScreen()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// The Invoke message delegate.
        /// </summary>
        /// <param name="clientName">The client name<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        /// <param name="isClientMessage">Checks if client message or server message<see cref="bool"/></param>
        /// <param name="isSuccess">Success status of message<see cref="bool"/></param>
        internal delegate void InvokeMessageDelegate(string clientName, string message, bool isClientMessage, bool isSuccess);

        /// <summary>
        /// The Invoke image delegate.
        /// </summary>
        /// <param name="source">The source<see cref="object"/></param>
        /// <param name="e">The event<see cref="ImageEventArgs"/></param>
        internal delegate void InvokeImageDelegate(object source, ImageEventArgs e);

        /// <summary>
        /// The Image error handling delegate.
        /// </summary>
        /// <param name="source">The source<see cref="object"/></param>
        /// <param name="e">The event<see cref="Masti.ImageProcessing.ErrorEventArgs"/></param>
        internal delegate void ImageErrorHandlerDelegate(object sender, Masti.ImageProcessing.ErrorEventArgs e);

    }
}

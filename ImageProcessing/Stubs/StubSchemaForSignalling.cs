﻿// -----------------------------------------------------------------------
// <author> 
//      Axel James
// </author>
//
// <date> 
//      16-11-2018 
// </date>
// 
// <reviewer>
//      Suman Panda
// </reviewer>
//
// <copyright file="StubSchemaForSignalling.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file is a part of ImageProcessing Module.
//      File contains implementaion of ISChema interface for unit testing.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.ImageProcessing.Stubs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    
    using Masti.Schema;

    /// <summary>
    /// This class implements ISchema interface for unit testing.
    /// </summary>
    public class StubSchemaForSignalling : ISchema
    {
        /// <summary>
        /// Simulates Schema modules decode function for unit testing
        /// </summary>
        /// <param name="data">data to be decoded</param>
        /// <param name="partialDecoding">decode type</param>
        /// <returns>dictionary with signal and imageData keys</returns>
        public IDictionary<string, string> Decode(string data, bool partialDecoding)
        {
            IDictionary<string, string> dictionary = new Dictionary<string, string>();
            if(data != null)
            {
                if (data.Equals("start", StringComparison.OrdinalIgnoreCase))
                {
                    dictionary.Add("signal", "Start");
                    dictionary.Add("imageData", "null");
                }
                else if (data.Equals("stop", StringComparison.OrdinalIgnoreCase))
                {
                    dictionary.Add("signal", "Stop");
                    dictionary.Add("imageData", "null");
                }
                else if (data.Equals("resend", StringComparison.OrdinalIgnoreCase))
                {
                    dictionary.Add("signal", "Resend");
                    dictionary.Add("imageData", "null");
                }
                else if (data.Equals("image", StringComparison.OrdinalIgnoreCase))
                {
                    dictionary.Add("signal", "Image");
                    dictionary.Add("imageData", "null");
                }
            }
            
            return dictionary;
        }

        /// <summary>
        /// Simulates Schema modules encode function for unit testing
        /// </summary>
        /// <param name="tagDict">a dictionary of with signal tag</param>
        /// <returns>value of signal key</returns>
        public string Encode(Dictionary<string, string> tagDict)
        {
            if (tagDict != null)
            {
                if (tagDict.ContainsKey("signal"))
                {
                    return tagDict["signal"];
                }
            }

            return "image";
        }
    }
}

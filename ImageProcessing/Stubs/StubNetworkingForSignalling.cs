﻿// -----------------------------------------------------------------------
// <author> 
//      Axel James
// </author>
//
// <date> 
//      16-11-2018 
// </date>
// 
// <reviewer>
//      Suman Panda
// </reviewer>
//
// <copyright file="StubNetworkingForSignalling.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file is a part of ImageProcessing Module.
//      File contains implementaion of Networking interface for unit testing.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.ImageProcessing.Stubs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.ImageProcessing.UnitTests;
    using Masti.Networking;

    /// <summary>
    /// This class implements Networking interface for unit testing.
    /// </summary>
    public class StubNetworkingForSignalling : ICommunication
    {
        public string LocalIP => "";

        public bool Connected => true;

        /// <summary>
        /// Simulates send function of Networking module and marks signals in SignalTestHelper
        /// </summary>
        /// <param name="msg">message string</param>
        /// <param name="targetIP">target ip</param>
        /// <param name="type">expecting Image type in test</param>
        /// <returns>always returns true</returns>
        public bool Send(string msg, IPAddress targetIP, DataType type)
        {
            SignallingTestHelper helper = SignallingTestHelper.Instance;
            if (msg != null)
            {
                if (msg.Equals("start", StringComparison.OrdinalIgnoreCase))
                {
                    helper.StartSendSuccessful = true;
                }
                else if (msg.Equals("stop", StringComparison.OrdinalIgnoreCase))
                {
                    helper.StopSendSuccessful = true;
                }
                else if (msg.Equals("resend", StringComparison.OrdinalIgnoreCase))
                {
                    helper.ResendSendSuccessful = true;
                }
                else if (msg.Equals("image", StringComparison.OrdinalIgnoreCase))
                {
                    helper.ImageSendSuccessful = true;
                }
            }

            return true;
        }

        public void StopCommunication()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Simulates subscribing to networking module and indicate it in helper class
        /// </summary>
        /// <param name="type">Image type for image module</param>
        /// <param name="receivalHandler">call back function</param>
        /// <returns>always true</returns>
        public bool SubscribeForDataReceival(DataType type, DataReceivalHandler receivalHandler)
        {
            SignallingTestHelper helper = SignallingTestHelper.Instance;
            helper.SubscribedToNetworking = true;
            return true;
        }

        /// <summary>
        /// shows error status
        /// </summary>
        /// <param name="type">type of data</param>
        /// <param name="statusHandler">handler for event</param>
        /// <returns>always true</returns>
        public bool SubscribeForDataStatus(DataType type, DataStatusHandler statusHandler)
        {
            return true;
        }
    }
}

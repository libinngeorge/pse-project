﻿// -----------------------------------------------------------------------
// <author> 
//      Axel James
// </author>
//
// <date> 
//      16-11-2018 
// </date>
// 
// <reviewer>
//      Suman Panda
// </reviewer>
//
// <copyright file="SignallingUnitTest.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file is a part of ImageProcessing Module.
//      File implements ITest and runs unit test of Signalling sub-module.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.ImageProcessing.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.ImageProcessing.Stubs;
    using Masti.ImageProcessing;
    using Masti.Networking;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// Implements ITest and runs unit test of Signalling sub-module.
    /// </summary>
    public class SignallingUnitTest : ITest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SignallingUnitTest" /> class.
        /// </summary>
        /// <param name="logger">Logger object used for logging during test</param>
        public SignallingUnitTest(ILogger logger)
        {
            this.Logger = logger;
        }

        /// <summary>
        /// Gets or sets Logger instance.
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// Tests methods of signalling submodule
        /// </summary>
        /// <returns>true if all tests passes else false</returns>
        public bool Run()
        {
            ICommunication communication = new StubNetworkingForSignalling();
            ISchema schema = new StubSchemaForSignalling();
            ImageCommunication imageCommunication = new ImageCommunication(communication, schema);
            SignallingTestHelper helper = SignallingTestHelper.Instance;

            this.TestSubscription(imageCommunication, helper);

            // Check if proper notifications are send when ReceiveSignalAndNotify is called
            IPAddress ipaddress = IPAddress.Parse("127.0.0.1");
            imageCommunication.ReceiveSignalAndNotify("start", ipaddress);
            imageCommunication.ReceiveSignalAndNotify("stop", ipaddress);
            imageCommunication.ReceiveSignalAndNotify("resend", ipaddress);
            imageCommunication.ReceiveSignalAndNotify("image", ipaddress);

            imageCommunication.ReceiveSignalAndNotify("not in list", ipaddress);
            if (!imageCommunication.ReceiveErrorStatus)
            {
                this.Logger.LogError("Cannot detect invalid signals");
                return false;
            }

            this.Logger.LogInfo("method ReceiveSignalAndNotify() throws errors for invalid case");

            // check if networking module is getting messages when signalled
            imageCommunication.SignalImageModule(ipaddress, Signal.Image);
            imageCommunication.SignalImageModule(ipaddress, Signal.Start);
            imageCommunication.SignalImageModule(ipaddress, Signal.Stop);
            imageCommunication.SignalImageModule(ipaddress, Signal.Resend);

            if (helper.CheckStatus())
            {
                this.Logger.LogSuccess("All checks are working");
                return true;
            }
            else
            {
                this.Logger.LogError("Status not set");
                return false;
            }
        }

        /// <summary>
        /// Checks if subscriptions are done.
        /// </summary>
        /// <param name="imageCommunication">ImageCommunication class object</param>
        /// <param name="helper">SignallingTestHelper class object</param>
        /// <returns>true if all subscriptions are successful</returns>
        private bool TestSubscription(ImageCommunication imageCommunication, SignallingTestHelper helper)
        {
            // Test start signal subscription
            bool startSubscriptionStatus = imageCommunication.SubscribeForSignalReceival(Signal.Start, helper.StartHandler);
            if (startSubscriptionStatus)
            {
                helper.StartSubscriptionCount = helper.StartSubscriptionCount + 1;
            }
            else
            {
                this.Logger.LogError("subscription failed for start signal");
                return false;
            }

            // Test stop signal subscription
            bool stopSubscriptionStatus = imageCommunication.SubscribeForSignalReceival(Signal.Stop, helper.StopHandler);
            if (stopSubscriptionStatus)
            {
                helper.StopSubscriptionCount = helper.StopSubscriptionCount + 1;
            }
            else
            {
                this.Logger.LogError("subscription failed for stop signal");
                return false;
            }

            // Test resend signal subscription
            bool resendSubscriptionStatus = imageCommunication.SubscribeForSignalReceival(Signal.Resend, helper.ResendHandler);
            if (resendSubscriptionStatus)
            {
                helper.ResendSubscriptionCount = helper.ResendSubscriptionCount + 1;
            }
            else
            {
                this.Logger.LogError("subscription failed for resend signal");
                return false;
            }

            // Test image subscription
            bool imageSubscriptionStatus = imageCommunication.SubscribeForSignalReceival(Signal.Image, helper.ImageHandler);
            if (imageSubscriptionStatus)
            {
                helper.ImageSubscriptionCount = helper.ImageSubscriptionCount + 1;
            }
            else
            {
                this.Logger.LogError("subscription failed for image signal");
                return false;
            }

            this.Logger.LogSuccess("subscriptions are successful");
            return true;
        }
    }
}

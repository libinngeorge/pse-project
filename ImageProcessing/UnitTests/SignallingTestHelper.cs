﻿// -----------------------------------------------------------------------
// <author> 
//      Axel James
// </author>
//
// <date> 
//      16-11-2018 
// </date>
// 
// <reviewer>
//      Suman Panda
// </reviewer>
//
// <copyright file="SignallingTestHelper.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file is a part of ImageProcessing Module.
//      File maintains states of signalling sub-module methods called.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.ImageProcessing.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.ImageProcessing.UnitTests;
    using Masti.Networking;

    /// <summary>
    /// Class maintains states of signalling sub-module methods called.
    /// </summary>
    public sealed class SignallingTestHelper
    {
        /// <summary>
        /// Gets a padlock object
        /// </summary>
        private static readonly object Padlock = new object();

        /// <summary>
        /// Store instance of the class
        /// </summary>
        private static SignallingTestHelper instance = null;

        /// <summary>
        /// Made true when subscriber for image is called
        /// </summary>
        private bool imageSubscriberCalled;

        /// <summary>
        /// Made true when subscriber for stop signal is called
        /// </summary>
        private bool stopSubscriberCalled;

        /// <summary>
        /// Made true when subscriber for start signal is called
        /// </summary>
        private bool startSubscriberCalled;

        /// <summary>
        /// Made true when subscriber for resend signal is called
        /// </summary>
        private bool resendSubscriberCalled;

        /// <summary>
        /// Prevents a default instance of the SignallingTestHelper class from being created.
        /// </summary>
        private SignallingTestHelper()
        {
            this.ImageSubscriptionCount = 0;
            this.StartSubscriptionCount = 0;
            this.StopSubscriptionCount = 0;
            this.ResendSubscriptionCount = 0;
            this.resendSubscriberCalled = false;
            this.imageSubscriberCalled = false;
            this.stopSubscriberCalled = false;
            this.resendSubscriberCalled = false;
            this.SubscribedToNetworking = false;
            this.ImageSendSuccessful = false;
            this.StartSendSuccessful = false;
            this.StopSendSuccessful = false;
            this.ResendSendSuccessful = false;
        }

        /// <summary>
        /// Gets a singleton instance of the class
        /// </summary>
        public static SignallingTestHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (Padlock)
                    {
                        if (instance == null)
                        {
                            instance = new SignallingTestHelper();
                        }
                    }
                }

                return instance;
            }
        }

        /// <summary>
        ///  Gets or sets a value indicating whether how many times subscription to Image signal happened.
        /// </summary>
        public int ImageSubscriptionCount { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether how many times subscription to Start signal happened.
        /// </summary>
        public int StartSubscriptionCount { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether how many times subscription to Stop signal happened.
        /// </summary>
        public int StopSubscriptionCount { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether how many times subscription to Resend signal happened.
        /// </summary>
        public int ResendSubscriptionCount { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether how many times subscription to Networking module was done.
        /// </summary>
        public bool SubscribedToNetworking { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether image send was successful.
        /// </summary>
        public bool ImageSendSuccessful { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether start signal was successful.
        /// </summary>
        public bool StartSendSuccessful { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether stop signal was successful.
        /// </summary>
        public bool StopSendSuccessful { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether resend signal was successful.
        /// </summary>
        public bool ResendSendSuccessful { get; set; }

        /// <summary>
        /// sets startSubscriberCalled
        /// </summary>
        /// <param name="data">data received</param>
        /// <param name="fromIP">ip of sender</param>
        public void StartHandler(string data, IPAddress fromIP)
        {
            this.startSubscriberCalled = true;
        }

        /// <summary>
        /// sets stopSubscriberCalled
        /// </summary>
        /// <param name="data">data received</param>
        /// <param name="fromIP">ip of sender</param>
        public void StopHandler(string data, IPAddress fromIP)
        {
            this.stopSubscriberCalled = true;
        }

        /// <summary>
        /// sets imageSubscriberCalled
        /// </summary>
        /// <param name="data">data received</param>
        /// <param name="fromIP">ip of sender</param>
        public void ImageHandler(string data, IPAddress fromIP)
        {
            this.imageSubscriberCalled = true;
        }

        /// <summary>
        /// sets resendSubscriberCalled
        /// </summary>
        /// <param name="data">data received</param>
        /// <param name="fromIP">ip of sender</param>
        public void ResendHandler(string data, IPAddress fromIP)
        {
            this.resendSubscriberCalled = true;
        }

        /// <summary>
        /// Checks if all variables are set
        /// </summary>
        /// <returns>True if all variables are set else false</returns>
        public bool CheckStatus()
        {
            if ((this.ImageSubscriptionCount > 0)  &&
                (this.StartSubscriptionCount > 0)  &&
                (this.StopSubscriptionCount > 0)   &&
                (this.ResendSubscriptionCount > 0) &&
                this.startSubscriberCalled         &&
                this.resendSubscriberCalled        &&
                this.imageSubscriberCalled         &&
                this.stopSubscriberCalled          &&
                this.resendSubscriberCalled        &&
                this.SubscribedToNetworking        &&
                this.ImageSendSuccessful           &&
                this.StartSendSuccessful           &&
                this.StopSendSuccessful            &&
                this.ResendSendSuccessful)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

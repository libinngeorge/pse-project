﻿//-----------------------------------------------------------------------
// <author> 
//     Suman Saurav Panda
// </author>
//
// <date> 
//     18-Nov-2018 
// </date>
// 
// <reviewer> 
//     Amish Ranjan
// </reviewer>
// 
// <copyright file="StubSchema.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This is a class intended to provide Test Cases for class ReceiveImage
//      and uses the ITest Interface. There are some saved images in specs directory
//      which are being used for testing the module.
//      For log specifications refer to QualityAssurance.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.ImageProcessing
{
    
    using System;
    using System.Collections.Generic;
    using Masti.Schema;

    /// <summary>
    /// class intended to give the stub schema for testing
    /// </summary>
    public class StubSchema : ISchema
    {
        /// <summary>
        /// dummy decode function for testing
        /// </summary>
        /// <param name="data">stimulus data for different results in testing</param>
        /// <param name="partialDecoding">dummy variable required to implement testing</param>
        /// <returns>returns a idictionary as required by the receiveImage class for different testing purpose</returns>
        public IDictionary<string, string> Decode(string data, bool partialDecoding)
        {
            var dummyDictionary = new Dictionary<string, string>();
            if (data == "true")
            {
                dummyDictionary.Add("implementDiff", "true");
                dummyDictionary.Add("imageDictionary", "true");
            }
            else if (data == "ex_schema")
            {
                throw new NotImplementedException();
            }
            else if (data == "ex_compress")
            {
                dummyDictionary.Add("implementDiff", "true");
                dummyDictionary.Add("imageDictionary", data);
            }
            else if (data == "ex_bmpstring")
            {
                dummyDictionary.Add("implementDiff", "true");
                dummyDictionary.Add("imageDictionary", data);
            }

            return dummyDictionary;
        }

        /// <summary>
        /// dummy function not used in testing for server side hence not implemented
        /// </summary>
        /// <param name="tagDict">dummy variable</param>
        /// <returns>not implemented method</returns>
        public string Encode(Dictionary<string, string> tagDict)
        {
            throw new NotImplementedException();
        }
    }
}
